# README #

### Como usar ###

OBS: No windows colocar o caminho completo

Ao fazer o clone alterar o path no arquivo bootstrap.sh ou fazer o clone com o mesmo nome de pasta raiz

Variável VANGRANT_BOOTSTRAP_PATH="/vagrant/bootstrap"

Variável VANGRANT_MAIN_PATH="/vagrant"

Onde /vagrant é a pasta raiz

Como na versão do ubuntu 14 o document root foi alterado para /var/www/html

VANGRANT_WWW_PATH = "/var/www/html"


Após feita a configuração entrar no diretório e rodar  -> vagrant up

para suspender a máquina é -> vagrant suspend

para voltar da suspensão -> vagrant resume

para desligar -> vagrant halt